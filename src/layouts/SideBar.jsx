import React from "react";
import { Link } from "react-router-dom";
import { Icon, Image, Menu, Sidebar } from "semantic-ui-react";

export default function SideBar() {
  return (
    <Sidebar
      as={Menu}
      animation="overlay"
      icon="labeled"
      inverted
      vertical
      visible
      width="thin"
    >
      <Menu.Item as="a">
        <Link to={"/tasklist"}>
          <Image src={require("../images/proopslogo.png")} />
        </Link>
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="tasks" />
        <Link to={"/tasklist"}>Görev Listesi </Link>
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="plus" fluid size="large" />
        Proje Oluşturma
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="plus" />
        <Link to={"/taskmanagement"}>Görev Oluşturma</Link>
      </Menu.Item>

      <Menu.Item as="a" style={{ marginTop: 400 }}>
        <Link to={"/"}>Çıkış Yap</Link>
      </Menu.Item>
    </Sidebar>
  );
}
