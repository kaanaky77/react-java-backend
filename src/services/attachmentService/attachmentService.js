import axios from "axios";

export default function attachmentService() {
  //const API_URL = "http://localhost:8080/api/upload";
  const API_URL = "http://localhost:8080/api/getCommentWithAttachments";
  return {
    /*postFile: (formData) => {

      return axios
        .post(API_URL, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
          },
        })
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };*/

  getCommentWithAttachments: () => {
    return axios
      .get(API_URL)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  getCommentById: (id) => {
    const url = `${API_URL}/${id}`;
    return axios
      .get(url)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  
};
}
