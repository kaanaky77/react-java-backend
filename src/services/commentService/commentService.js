import axios from "axios";

export default function commentService() {
  const API_URL = "http://localhost:8080/api/getComment";

  return {    
    getComments: () => {
      return axios
        .get(API_URL)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
