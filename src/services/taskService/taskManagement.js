import axios from "axios";

export default function taskManagement() {
  return {
    addTask: (data) => {
      console.log(data);
      return axios
        .post("http://localhost:8080/api/addTask", data)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
    getTask: (id) => {
      console.log(id);
      return axios
        .get("http://localhost:8080/api/getByIdTaskManagement?taskId="+id)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

  };
}
