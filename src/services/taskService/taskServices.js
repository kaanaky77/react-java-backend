import axios from "axios";

export default function taskServices() {
  return {
    getTaskById: (id) => {
      
      return axios
        .get("http://localhost:8080/api/getTaskById?id="+id["id"])
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
