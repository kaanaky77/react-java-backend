import axios from "axios";

export default function loginService() {
  const API_URL = "http://localhost:8080/api/login";

  return {
    postLogin: (data) => {
      return axios
        .post(API_URL, data)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
