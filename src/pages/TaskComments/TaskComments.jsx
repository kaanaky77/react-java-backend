import React, { useState, useEffect } from "react";
import { Comment } from 'semantic-ui-react';
import {
  Breadcrumb,
  Divider,
  Form,
  Segment,
  Container,
  Checkbox,
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import "./TaskComments.css";
import attachmentService from "../../services/attachmentService/attachmentService";
import commentService from "../../services/commentService/commentService";
import _ from "lodash";

export default function TaskComments() {
  const sections = [
    { key: "Proje Adı", content: "Proje Adı", link: true },
    { key: "Faz", content: "Faz", link: true },
    { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
    { key: "İş paketi", content: "İş paketi", link: true },
    { key: "GörevID_Görev", content: "GörevID_Görev", link: true },
    { key: "Görev Yorumları", content: "Görev Yorumları", active: true },
  ];

  const handleFileChange = (e) => {
    console.log(e.target.value);
    const formData = new FormData();
    formData.append("file", e.target.files[0]);

    attachmentService()
      .postFile(formData)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };    

        

      /*useEffect(() => {
        taskServices()
          .getAllTasks()
          .then((response) => {
            dispatch({ type: "SET_DATA", payload: response });
          })
          .catch((error) => {
            console.error(error);
          });
      }, []);*/

      //const { column, data1, direction } = state;
      const [commentt] = useState("");
     const [comments,  setComments] = useState([]);
     useEffect(() => {
      // Fetch comments from server using the getComments function
      commentService()
        .getComments()
        .then((data) => {
          // Set the comments state to the array of comments returned by the server
          setComments(data);
        })
        .catch((error) => {
          console.log(error);
        });
    }, []);

    

  
  return (
    <div>
      <SideBar />
      <Container>
        <Breadcrumb icon="right angle" sections={sections} />
        <Divider />
        <Form onSubmit={sections}>
        <Form.Group>
        <Form.Input type="file" icon="attach" onChange={handleFileChange} />
        <Form.TextArea
              width="16"
              placeholder="Yorum ekleyiniz..."
              value={commentt}
              onChange={(e) => {
                
              }}
            />
          </Form.Group>
          <Form.Button
            icon="plus circle"
            labelPosition="right"
            content="Yorum Ekle"
            floated="right"
          />
        </Form>
        <Checkbox label="Efor ?" />

        <div>
          <p>
            <input type="datetime-local" id="datetime" name="datetime" />
          </p>
          <p>
            <input type="datetime-local" id="datetime" name="datetime" />
          </p>
        </div>

        <Segment basic textAlign="center" disabled>
          <Divider />
        </Segment>

        <Comment.Group>
        {comments.map((comment, index) => (
        <Comment key={index}>
          <Comment.Avatar src="https://react.semantic-ui.com/images/avatar/small/stevie.jpg" />
          <Comment.Content>
            <Comment.Author>Bircan Ataş - AVE</Comment.Author>
            <Comment.Metadata>
              <div>2 gün önce</div>
            </Comment.Metadata>
            <Comment.Text>
              <p>{comment.yorumDetay}</p>
            </Comment.Text>
          </Comment.Content>
        </Comment>
      ))}
    </Comment.Group>
      </Container>
    </div>
  );
 };


