import React, { useEffect } from "react";
import { Container, Dropdown, Pagination, Table } from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import _ from "lodash";
import taskServices from "../../services/taskService/taskServices";
import { useLocation } from 'react-router-dom';

export default function TaskList() {
  const titleList = [
    { key: "1", text: "Görev Yönetici", value: "Görev Yönetici" },
    { key: "2", text: "Danışman", value: "Danışman" },
    { key: "3", text: "Proje Yöneticisi", value: "Proje Yöneticisi" },
    { key: "4", text: "Sorumlu", value: "Sorumlu" },
    { key: "5", text: "Teknik Lider", value: "Teknik Lider" },
    { key: "6", text: "Ürün Sahibi", value: "Ürün Sahibi" },
  ];

  const location = useLocation();
  const email = location.state.email;
  const id = location.state.id;


  return (
    <div>
      <SideBar />
      <div>
        <Container className="">
          <Dropdown
            placeholder="Görev Listesi"
            fluid
            multiple
            selection
            options={titleList}
            clearable
            wrapSelection={false}
          />
          <section>
            <SortableTable id={id}/>
          </section>
          <footer style={{ display: "flex", justifyContent: "center" }}>
            <Pagination defaultActivePage={5} totalPages={10} />
          </footer>
        </Container>
      </div>
    </div>
  );
}

function exampleReducer(state, action) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      } else {
        return {
          column: action.column,
          data: _.sortBy(state.data, [action.column]),  
          direction: "ascending",
        };
      }

    case "SET_DATA":
      return {
        ...state,
        data: action.payload,
      };
    case "SET_COLUMN":
      return {
        ...state,
        column: action.payload.column,
        direction: action.payload.direction,
      };
    default:
      throw new Error();
  }
}

function SortableTable(id) {
  console.log("SortableTable id :", id);
  const [state, dispatch] = React.useReducer(exampleReducer, {
    column: null,
    data: [],
    direction: null,
  });

  useEffect(() => {
    taskServices()
      .getTaskById(id)
      .then((response) => {
        dispatch({ type: "SET_DATA", payload: response });
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const { column, data, direction } = state;

  return (
    <Table sortable celled fixed>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell
            sorted={column === "taskid" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "taskid" })}
          >
            Görev ID
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "name" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "name" })}
          >
            Görev Başlığı
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "age" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "age" })}
          >
            Son Tarih
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "gender" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "gender" })}
          >
            Öncelik
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {data.map(
          ({ planned_end_date, priority_name, task_id, task_name }) => (
            <Table.Row key={task_id}>
              <Table.Cell>{task_id}</Table.Cell>
              <Table.Cell>{task_name}</Table.Cell>
              <Table.Cell>{planned_end_date}</Table.Cell>
              <Table.Cell>{priority_name}</Table.Cell>
            </Table.Row>
          )
        )}
      </Table.Body>
    </Table>
  );
}
