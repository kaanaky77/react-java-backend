import React, { useState, useEffect } from "react";
import { Calendar } from "primereact/calendar";
import "primereact/resources/themes/md-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import {
  Breadcrumb,
  Button,
  Divider,
  Dropdown,
  Form,
  Message,
  TextArea,
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import { Link, useLocation } from "react-router-dom";
import taskManagement from "../../services/taskService/taskManagement";

export default function TaskManagement() {
  // const location = useLocation();
  // const id = location.state.id;
  // console.log("managament id:", id);

  const titleList = [
    { key: "1", text: "Görev Yönetici", value: 1 },
    { key: "2", text: "Danışman", value: 2 },
    { key: "3", text: "Proje Yöneticisi", value: 3 },
    { key: "4", text: "Sorumlu", value: 4 },
    { key: "5", text: "Teknik Lider", value: 5 },
    { key: "6", text: "Ürün Sahibi", value: 6 },
  ];

  const projectManager = [
    { key: "1", text: "Harun Çetin", value: 1 },
    { key: "2", text: "Tuba Öngören", value: 2 },
    { key: "3", text: "Hülya Keleş", value: 3 },
    { key: "4", text: "Bircan Ataş", value: 4 },
  ];

  const taskTypeList = [
    { key: "1", text: "Fırsatlar", value: "Fırsatlar" },
    { key: "2", text: "Ziyaretler", value: "Ziyaretler" },
    { key: "3", text: "Şartname Hazırlık", value: "Şartname Hazırlık" },
    { key: "4", text: "Poc / Demo", value: "Poc / Demo" },
  ];

  const priorityList = [
    { key: "1", text: "Çok Acil", value: "Çok Acil" },
    { key: "2", text: "Acil", value: "Acil" },
    { key: "3", text: "Normal", value: "Normal" },
    { key: "4", text: "Bekletilebilir", value: "Bekletilebilir" },
    { key: "5", text: "İstendiği Zaman", value: "İstendiği Zaman" },
  ];

  const situationList = [
    {
      key: "1",
      text: "Beklemede(Eksik atama)",
      value: "Beklemede(Eksik atama)",
    },
    {
      key: "2",
      text: "Planlandı (Atamalar tamamlanmış)",
      value: "Planlandı (Atamalar tamamlanmış)",
    },
    { key: "3", text: "Çalışılıyor", value: "Çalışılıyor" },
  ];

  const progressList = [];

  const sections = [
    { key: "Proje Adı", content: "Proje Adı", link: true },
    { key: "Faz", content: "Faz", link: true },
    { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
    { key: "İş paketi", content: "İş paketi", link: true },
    { key: "GörevID_Görev", content: "GörevID_Görev", active: true },
  ];

  // project_id gelecek diğer sayfadan

  const [taskName, setTaskName] = useState("");
  const [projectManagerId, setProjectManagerId] = useState("");
  const [responsibleId, setResponsibleId] = useState("");
  const [advisorId, setAdvisorId] = useState("");
  const [productOwner, setProductOwner] = useState("");
  const [technicalLeaderId, setTechnicalLeaderId] = useState("");
  const [plannedStartDate, setPlannedStartDate] = useState(new Date());
  const [plannedEndDate, setPlannedEndDate] = useState(new Date());

  const [actualStartDate, setActualStartDate] = useState(new Date());
  const [actualEndDate, setActualEndDate] = useState(new Date());

  const [plannedEffort, setPlannedEffort] = useState("");
  const [effortMade, setEffortMade] = useState("");
  const [taskType, setTaskType] = useState("");
  const [priority, setPriority] = useState("");
  const [situation, setSituation] = useState("");
  const [taskDescription, setTaskDescription] = useState("");

  const [message, setMessage] = useState(null);
  const handleSubmit = (e) => {
    e.preventDefault();

    const data = {
      taskName: taskName,
      projectManagerId: projectManagerId,
      responsibleId: responsibleId,
      advisorId: advisorId,
      productOwner: productOwner,
      technicalLeaderId: technicalLeaderId,
      taskType: taskType,
      priority: priority,
      situation: situation,
      plannedStartDate: plannedStartDate,
      plannedEndDate: plannedEndDate,
      plannedEffort: plannedEffort,
      actualStartDate: actualStartDate,
      actualEndDate: actualEndDate,
      effortMade: effortMade,
      taskDescription: taskDescription,
    };

    taskManagement()
      .addTask(data)
      .then((response) => {
        console.log("response:", response);
        showMessage(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const showMessage = (message) => {
    setMessage(message);
    setTimeout(() => {
      setMessage(null);
    }, 3000); // 3 saniye beklet
  };

  var getTaskData = {
    taskName: "",
    projectManagerId: "",
    responsibleId: "",
    advisorId: "",
    productOwner: "",
    technicalLeaderId: "",
    taskType: "",
    priority: "",
    situation: "",
    plannedStartDate: "",
    plannedEndDate: "",
    plannedEffort: "",
    actualStartDate: "",
    actualEndDate: "",
    effortMade: "",
    taskDescription: "",
  };

  const [disable, setDisable] = useState(false);
  useEffect((id) => {
    taskManagement()
      .getTask(1)
      .then((response) => {
        console.log(response);
        setTaskName(response[0].task_name);
        setProjectManagerId(response[0].project_manager_id);
        setResponsibleId(response[0].responsible_id);
        setAdvisorId(response[0].advisor_id);
        setProductOwner(response[0].product_owner_id);
        setTechnicalLeaderId(response[0].technical_leader_id);

              
        // setPlannedStartDate(response[0].planned_start_date);
        
        const dateArray = response[0].planned_start_date.split("/");
        const year = dateArray[2];
        const month = dateArray[1] - 1; // JavaScript'te ay sayıları 0'dan başlar
        const day = dateArray[0];
        const plannedStartDate = new Date(year, month, day);
        setPlannedStartDate(plannedStartDate)
        
        setPlannedEndDate(response[0].planned_end_date);
        setActualStartDate(response[0].actual_start_date);
        setActualEndDate(response[0].actual_end_date);
        setPlannedEffort(response[0].planned_effort);
        setEffortMade(response[0].effort_made);
        // setTaskType(response[0].task_name)
        setPriority(response[0].priority_name);
        setSituation(response[0].situation_name);
        setTaskDescription(response[0].task_description);
        setDisable(true);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <div>
      <SideBar />
      <Breadcrumb icon="right angle" sections={sections} />
      <Divider />
      <Form>
        <Form.Input
          placeholder="Başlık"
          style={{ width: "1340px" }}
          value={taskName}
          disabled={disable}
          onChange={(e) => setTaskName(e.target.value)}
        />
      </Form>
      <div style={{ display: "flex", marginTop: "20px" }}>
        <Dropdown
          placeholder="Proje Yöneticisi"
          fluid
          selection
          options={projectManager}
          value={projectManagerId}
          disabled={disable}
          onChange={(e, data) => setProjectManagerId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Görev Sorumlusu"
          fluid
          selection
          options={projectManager}
          disabled={disable}
          value={responsibleId}
          onChange={(e, data) => setResponsibleId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Teknik Lider"
          fluid
          selection
          options={projectManager}
          disabled={disable}
          value={technicalLeaderId}
          onChange={(e, data) => setTechnicalLeaderId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Danışman"
          fluid
          selection
          options={projectManager}
          disabled={disable}
          value={advisorId}
          onChange={(e, data) => setAdvisorId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Ürün Sorumlusu"
          fluid
          selection
          options={projectManager}
          value={productOwner}
          disabled={disable}
          onChange={(e, data) => setProductOwner(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Dropdown
          placeholder="Görev Türü"
          fluid
          selection
          options={taskTypeList}
          clearable
          wrapSelection={false}
          disabled={disable}
          value={taskType}
          onChange={(e, data) => setTaskType(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder="Öncelik"
          fluid
          selection
          options={priorityList}
          clearable
          wrapSelection={false}
          disabled={disable}
          value={priority}
          onChange={(e, data) => setPriority(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder="Durum"
          fluid
          selection
          options={situationList}
          clearable
          wrapSelection={false}
          value={situation}
          disabled={disable}
          onChange={(e, data) => setSituation(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />

        <Dropdown
          placeholder="İlerleme Durumu"
          fluid
          selection
          options={projectManager}
          clearable
          disabled={disable}
          wrapSelection={false}
          style={{ width: "300px", marginTop: "40px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          value={plannedStartDate}
          onChange={(e) => setPlannedStartDate(e.target.value)}
          showIcon
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Başlangıç Tarihi"}
        />
        <Calendar
          value={plannedEndDate}
          onChange={(e) => setPlannedEndDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px", height: "40px" }}>
          <Form>
            <Form.Input
              placeholder="Planlanan Efor"
              value={plannedEffort}
              type="number"
              min="0"
              disabled={disable}
              onChange={(e) => setPlannedEffort(e.target.value)}
            />
          </Form>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          value={actualStartDate}
          onChange={(e) => setActualStartDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Başlangıç Tarihi"}
        />
        <Calendar
          value={actualEndDate}
          onChange={(e) => setActualEndDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px" }}>
          <Form>
            <Form.Input
              placeholder="Gerçekleşen Efor"
              type="number"
              min="0"
              disabled={disable}
              value={effortMade}
              onChange={(e) => setEffortMade(e.target.value)}
            />
          </Form>
        </div>
      </div>
      <div style={{ marginTop: "190px" }}>
        <Form>
          <TextArea
            placeholder="Buraya görev tanımı ekleyin"
            value={taskDescription}
            disabled={disable}
            onChange={(e) => setTaskDescription(e.target.value)}
          />
        </Form>
      </div>
      <br />
      <Button primary onClick={handleSubmit}>
        Görev Ekle
      </Button>

      {message && (
        <Message>
          <p>{message}</p>
        </Message>
      )}

      <br />
      <div style={{ display: "flex" }}>
        <Link to={"/preposttasks"}>
          <Button
            variant="contained"
            style={{ width: "620px", marginRight: "40px", marginTop: "30px" }}
          >
            Öncül veya Ardıl Görev Listesi
          </Button>
        </Link>
        {/* bu görevin id'si ile gitmeli */}
        <Link to={"/taskcomments"}>
          <Button
            variant="contained"
            style={{ width: "620px", marginLeft: "65px", marginTop: "30px" }}
          >
            Görev Yorumları
          </Button>
        </Link>
      </div>
    </div>
  );
}
