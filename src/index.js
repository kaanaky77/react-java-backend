import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import TaskList from "./pages/TaskLists/TaskList";
import TaskComments from "./pages/TaskComments/TaskComments";
import PrePostTasks from "./pages/PrePostTasks/PrePostTasks";
import TaskManagement from "./pages/TaskManagements/TaskManagement";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <React.StrictMode>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/tasklist" element={<TaskList />} />
        <Route path="/taskcomments" element={<TaskComments />} />
        <Route path="/preposttasks" element={<PrePostTasks />} />
        <Route path="/taskmanagement" element={<TaskManagement />} />
      </Routes>
    </React.StrictMode>
  </BrowserRouter>
);


reportWebVitals();
